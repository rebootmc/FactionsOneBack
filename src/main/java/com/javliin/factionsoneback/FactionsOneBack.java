package com.javliin.factionsoneback;

import com.earth2me.essentials.Essentials;
import com.massivecraft.factions.Board;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.struct.Rel;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class FactionsOneBack extends JavaPlugin implements Listener {
    private Essentials essentials;
    private HashMap<Rel, Boolean> allowed;

    public void onEnable() {
        essentials = (Essentials) getServer().getPluginManager().getPlugin("Essentials");
        allowed = new HashMap<Rel, Boolean>();

        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(this, this);

        allowed.put(Rel.ALLY, (Boolean) getConfig().get("ally"));
        allowed.put(Rel.ENEMY, (Boolean) getConfig().get("enemy"));
        allowed.put(Rel.NEUTRAL, (Boolean) getConfig().get("neutral"));
        allowed.put(Rel.TRUCE, (Boolean) getConfig().get("truce"));
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        if(!event.getMessage().equalsIgnoreCase("/back")) return;
        if(!event.getPlayer().hasPermission("essentials.back")) return;
        if(essentials.getUser(event.getPlayer()).getLastLocation() == null) return;

        if(!allowed.get(Board.getFactionAt(essentials.getUser(event.getPlayer()).getLastLocation()).getRelationTo(FPlayers.i.get(event.getPlayer()).getFaction()))) {
            event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("message.not-allowed")));
            event.setCancelled(true);
        }
    }
}
